/** @format */

import { ComonIcons } from "../../content/commonIcons";
import {
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Input,
  FormGroup,
} from "reactstrap";
import { connect, useDispatch } from "react-redux";
import { setadminPanelStatus } from "../../redux/actions/adminPanel/adminPanelAction";
import React, { Component } from "react";

class Navbar extends Component {
  state = {};
  render() {
    return (
      <div className="custom-navbar">
        <div className="navabar-brand-block">
          <div
            className="burger-menu c-pointer py-2  "
            onClick={() => {
              console.log("0", this.props.adminPanelFlag);
              this.props.setadminPanelStatus(!this.props.adminPanelFlag);
            }}
          >
            {ComonIcons.burger_menu}
          </div>
        </div>
        <div className="common-navbar-block">
          <ul className="list-wrapper">
            <li className="list-element">
              <FormGroup className="position-relative mb-0 ">
                <Input
                  type="text"
                  name="last-name"
                  id="last-name"
                  placeholder="Search box"
                  className="with-icons px-2"
                  // className="with-icons"
                  // value={this.state.lname}
                  // onChange={(e) => this.setData("lname", e.target.value)}
                  // onBlur={() => this.validator.showMessageFor("lname")}
                />
              </FormGroup>
            </li>

            {/* <li className="list-element notification-drp">
              <UncontrolledDropdown>
                <DropdownToggle tag="a" className="notification c-pointer">
                  {ComonIcons.notification_nav}
                </DropdownToggle>
                <DropdownMenu className="menu">
                  <DropdownItem header>Header</DropdownItem>
                  <DropdownItem disabled>Action</DropdownItem>
                  <DropdownItem>Another Action</DropdownItem>
                  <DropdownItem divider />
                  <DropdownItem>Another Action</DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
            </li> */}
            <li className="list-element user-drp">
              <UncontrolledDropdown>
                <DropdownToggle tag="a" className="user">
                  <img
                    src={require("../../assets/images/user.svg")}
                    className="img-fluid"
                    alt="user"
                  />
                </DropdownToggle>
                <DropdownMenu className="menu">
                  <DropdownItem>My Profile</DropdownItem>
                  <DropdownItem>Setting</DropdownItem>
                  <DropdownItem>Logout</DropdownItem>
                  {/* <DropdownItem divider />
                  <DropdownItem>Another Action</DropdownItem> */}
                </DropdownMenu>
              </UncontrolledDropdown>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("dvjsbjvbj", state);
  const { adminPanelFlag } = state.design;
  return {
    adminPanelFlag,
  };
};
export default connect(mapStateToProps, {
  setadminPanelStatus,
})(Navbar);
