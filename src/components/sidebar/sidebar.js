/** @format */

import { render } from "@testing-library/react";
import React, { Component } from "react";
import { Link, NavLink } from "react-router-dom";
import { ComonIcons } from "../../content/commonIcons";
import { connect, useDispatch } from "react-redux";
import { setadminPanelStatus } from "../../redux/actions/adminPanel/adminPanelAction";
class sidebar extends Component {
  render() {
    return (
      <div className="sidebar">
        <div className="sidebar-header sidebar-padding-x py-3">
          <div className="logo-block ">
            <h1>
              <span className="des-tx-secondary">L</span>
              <span className="des-tx-primary">ogo</span>
            </h1>
          </div>
          <div
            className="close-sidebar"
            onClick={() => {
              console.log("0", this.props.adminPanelFlag);
              this.props.setadminPanelStatus(!this.props.adminPanelFlag);
            }}
          >
            {ComonIcons.close}
          </div>
        </div>
        <div className="sidebar-body">
          <ul className="sidebar-list-link-wrapper">
            <li className="sidebar-link-wrappper">
              <NavLink to="/dashboard" className="path-link">
                <div className="sidebar-link">
                  <span className="block-1">{ComonIcons.dashboard}</span>
                  <span className="block-2">Home</span>
                </div>
              </NavLink>
            </li>
            <li className="sidebar-link-wrappper">
              <NavLink to="/dashboard-2" className="path-link">
                <div className="sidebar-link">
                  <span className="block-1">{ComonIcons.notificationLink}</span>
                  <span className="block-2">Trash</span>
                </div>
              </NavLink>
            </li>
          </ul>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  console.log("dvjsbjvbj", state);
  const { adminPanelFlag } = state.design;
  return {
    adminPanelFlag,
  };
};

export default connect(mapStateToProps, {
  setadminPanelStatus,
})(sidebar);
