import * as serviceWorker from './serviceWorker'
import React, { Suspense, lazy } from 'react'
import ReactDOM from 'react-dom'
import { Provider } from 'react-redux'
import { store } from './redux/storeConfig/store'
import { BrowserRouter } from 'react-router-dom'
import 'react-redux-toastr/lib/css/react-redux-toastr.min.css'
// import "./index.scss";
import 'bootstrap/dist/css/bootstrap.min.css'

import './assets/scss/style.scss'
import './assets/scss/responsive.scss'
import config from './config'
import ReduxToastr from 'react-redux-toastr'

import Spinner from './components/spinner/spinner'
const LazyApp = lazy(() => import('./app/AppRouter'))

const jsx = (
  <Provider store={store}>
    <BrowserRouter basename={config.basename}>
      <Suspense fallback={<Spinner />}>
        <LazyApp />
      </Suspense>
    </BrowserRouter>
    <ReduxToastr
      timeOut={4000}
      newestOnTop={false}
      preventDuplicates
      position='top-right'
      getState={state => state.toastr} // This is the default
      transitionIn='fadeIn'
      transitionOut='fadeOut'
      progressBar
      closeOnToastrClick
    />
  </Provider>
)

ReactDOM.render(jsx, document.getElementById('root'))

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: http://bit.ly/CRA-PWA
serviceWorker.unregister()
