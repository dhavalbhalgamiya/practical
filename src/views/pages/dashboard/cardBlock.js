/** @format */
import React, { useState } from "react";
import {
  Card,
  CardBody,
  CardImg,
  CardTitle,
  CardSubtitle,
  CardText,
  Button,
} from "reactstrap";
import EditModal from "./editProduct";
const CardMain = ({ updatedata, userdata, removeCard, editcard }) => {
  const [modal, setModal] = useState(false);
  const toggle = () => {
    console.log("1231321321");
    setModal(!modal);
  };
  return (
    <div>
      <Card className="mb-3 main-card">
        {/* <CardImg
          top
          width="100%"
          src="/assets/318x180.svg"
          alt="Card image cap"
        /> */}
        <CardBody>
          <CardTitle tag="h5">{userdata.title}</CardTitle>
          {/* <CardSubtitle tag="h6" className="mb-2 text-muted">
            {userdata.title}
          </CardSubtitle> */}
          <CardText>{userdata.body}</CardText>
          <Button
            className="me-2 btn-danger"
            onClick={() => removeCard(userdata.id)}
          >
            Remove Card
          </Button>
          <Button className="btn-success" onClick={toggle}>
            Edit Card
          </Button>
        </CardBody>
      </Card>
      <EditModal
        updatedata={updatedata}
        toggle={toggle}
        modal={modal}
        userdata={userdata}
      />
    </div>
  );
};

export default CardMain;
