import React from "react";
import { Row, Col, Card, CardBody, Form, FormGroup, Input } from "reactstrap";
import { ComonIcons } from "../../../content/commonIcons";
import { NavLink, Link } from "react-router-dom";
function ChangePassword() {
  return (
    <div>
      <div className="manage-edit-account">
        <div className="title Edit-title">
          <h1 className="f-700">Change Password</h1>
        </div>
        <div className="content py-3">
          <Form>
            <Col md={7}>
              <FormGroup className="position-relative">
                <Input
                  type="password"
                  name="password"
                  placeholder="Password"
                  className="with-icons"
                  // value={this.state.email}
                  // onChange={(e) => this.setData("email", e.target.value)}
                  // onBlur={() => this.validator.showMessageFor("email")}
                />
                {/* <span className="absolute-icon">{ComonIcons.mailer}</span>
                {this.state.activeTab === "2"
                  ? this.validator.message(
                      "email",
                      this.state.email,
                      "required|email",
                      { className: "text-danger" }
                    )
                  : null} */}
              </FormGroup>
            </Col>
            <Col md={7}>
              <FormGroup className="position-relative">
                <Input
                  type="password"
                  name="newpassword"
                  placeholder="New Password"
                  className="with-icons"
                  // value={this.state.email}
                  // onChange={(e) => this.setData("email", e.target.value)}
                  // onBlur={() => this.validator.showMessageFor("email")}
                />
                {/* <span className="absolute-icon">{ComonIcons.mailer}</span>
                {this.state.activeTab === "2"
                  ? this.validator.message(
                      "email",
                      this.state.email,
                      "required|email",
                      { className: "text-danger" }
                    )
                  : null} */}
              </FormGroup>
            </Col>
            <Col md={7}>
              <FormGroup className="position-relative">
                <Input
                  type="password"
                  name="confirmpassword"
                  placeholder="Confirm Password"
                  className="with-icons"
                  // value={this.state.email}
                  // onChange={(e) => this.setData("email", e.target.value)}
                  // onBlur={() => this.validator.showMessageFor("email")}
                />
                {/* <span className="absolute-icon">{ComonIcons.mailer}</span>
                {this.state.activeTab === "2"
                  ? this.validator.message(
                      "email",
                      this.state.email,
                      "required|email",
                      { className: "text-danger" }
                    )
                  : null} */}
              </FormGroup>
            </Col>
            <Col lg={3}>
              <div className="py-4">
                <button className="btn btn-custom-primary w-100">
                  Save Changes
                </button>
              </div>
            </Col>
          </Form>
        </div>
      </div>
    </div>
  );
}

export default ChangePassword;
