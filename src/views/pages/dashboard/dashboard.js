/** @format */

import React from "react";
import { Row, Col, Card, CardBody } from "reactstrap";
import { ChevronRight } from "react-feather";
import ProductList from "./productList";
function subscription() {
  return (
    <div>
      <div className="dashboard">
        <div className="breadcum d-flex  align-items-center">
          <h4 className="mb-0 f-700">Level 1</h4>
          <ChevronRight size={15} className="mx-2" />
          <h4 className="mb-0 f-700">Level 2</h4>
        </div>

        <div className="filters">
          <div className="filters-block">
            <span>
              <label class="custom-container ">
                Checkbox1
                <input type="checkbox" />
                <span class="checkmark"></span>
              </label>
            </span>
          </div>
          <div className="filters-block">
            <span>
              <label class="custom-container ">
                Checkbox2
                <input type="checkbox" />
                <span class="checkmark"></span>
              </label>
            </span>
          </div>
          <div className="filters-block">
            <span>
              <label class="custom-container ">
                Checkbox3
                <input type="checkbox" />
                <span class="checkmark"></span>
              </label>
            </span>
          </div>
          <div className="filters-block">
            <span>
              <label class="custom-container ">
                Checkbox4
                <input type="checkbox" />
                <span class="checkmark"></span>
              </label>
            </span>
          </div>
          <div className="filters-block">
            <span>
              <label class="custom-container ">
                Checkbox1
                <input type="checkbox" />
                <span class="checkmark"></span>
              </label>
            </span>
          </div>
        </div>

        <ProductList />
      </div>
    </div>
  );
}

export default subscription;
