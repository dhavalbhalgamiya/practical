/** @format */

import React, { useState } from "react";
import {
  Row,
  Col,
  Form,
  FormGroup,
  Label,
  Input,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
} from "reactstrap";

const EditModal = ({ updatedata, userdata, modal, toggle }) => {
  const [title, settitle] = useState(userdata.title);
  const [body, setbody] = useState(userdata.body);
  // const [modal, setModal] = useState(false);
  // const toggle = () => setModal(!modal);

  const editcard = (e) => {
    e.preventDefault();
    const newdata = {
      id: userdata.id,
      title,
      body,
    };
    updatedata(newdata);
  };
  console.log("modal");
  return (
    <div>
      <Modal isOpen={modal} toggle={toggle}>
        <ModalBody>
          <Row className="">
            <h1>Edit Card</h1>
          </Row>
          <Row className="my-5 justify-content-center">
            <Col md="12">
              <Form onSubmit={editcard}>
                <FormGroup className="mb-3">
                  <Label for="title">Title</Label>
                  <Input
                    type="text"
                    name="title"
                    value={title}
                    onChange={(e) => settitle(e.target.value)}
                    id="title"
                    placeholder="title"
                  />
                </FormGroup>
                <FormGroup className="mb-3">
                  <Label for="examplePassword">Content</Label>
                  <Input
                    type="textarea"
                    value={body}
                    onChange={(e) => setbody(e.target.value)}
                    name="bbody"
                    id="bbody"
                    placeholder="bbody"
                  />
                </FormGroup>
                <Button className="btn-regular" type="submit" onClick={toggle}>
                  Submit
                </Button>{" "}
                <Button color="danger" onClick={toggle}>
                  Cancel
                </Button>
              </Form>
            </Col>
          </Row>
        </ModalBody>
      </Modal>
    </div>
  );
};

export default EditModal;
