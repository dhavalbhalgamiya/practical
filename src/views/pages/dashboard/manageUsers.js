import {
  Row,
  Col,
  Card,
  CardBody,
  Form,
  FormGroup,
  Input,
  Button,
  Table,
} from "reactstrap";
import { ComonIcons } from "../../../content/commonIcons";
import { NavLink, Link } from "react-router-dom";
import { SketchPicker } from "react-color";
import reactCSS from "reactcss";
import React, { Component } from "react";
class ManageUsers extends Component {
  render() {
    return (
      <div className="manage-user">
        <div className="title Edit-title">
          <h1 className="f-700">Brand Settings</h1>
        </div>
        <div className="content ">
          <Row>
            <Col md={12}>
              <h5 className="des-tx-fifth pb-2">
                Social media is more fun with friends! Add team members to
                collaborate on Keyhole.
              </h5>
              <h5 className="des-tx-fifth pb-2">
                <span className="f-bold des-tx-black">Owner : </span> Can manage
                subscriptions, users and trackers. Email success@keyhole.co to
                change.
              </h5>
              <h5 className="des-tx-fifth pb-2">
                <span className="f-bold des-tx-black">Administrator : </span>{" "}
                Can manage users and trackers, but not subscription and billing
              </h5>
              <h5 className="des-tx-fifth pb-2">
                <span className="f-bold des-tx-black">Editor : </span> Can
                create, pause, restart trackers.
              </h5>
              <h5 className="des-tx-fifth pb-2">
                <span className="f-bold des-tx-black">Viewer : </span> Can only
                see trackers.
              </h5>
            </Col>
            <Col md={12}>
              <div className="d-flex align-items-center mt-3">
                <Button className="btn btn-custom-primary add-new">
                  Add New User
                </Button>

                <p className="ml-3 des-tx-fifth"> Used: 1 / 25</p>
              </div>
            </Col>
          </Row>
          <Row>
            <Col md={12}>
              <div className="manage-user-table">
                <table responsive>
                  <tr>
                    <th>User</th>
                    <th>Role</th>
                    <th>MOnthly Posts</th>
                  </tr>
                </table>
                <table class="gfg" responsive>
                  <tr>
                    <td>
                      <div>Thomas</div>
                      <div className="noted">its you</div>
                    </td>
                    <td>Owner</td>
                    <td>1000</td>
                  </tr>
                  <tr>
                    <td>
                      <div>Thomas</div>
                      <div className="noted">its you</div>
                    </td>
                    <td>Owner</td>
                    <td>1000</td>
                  </tr>
                  <tr>
                    <td>
                      <div>Thomas</div>
                      <div className="noted">its you</div>
                    </td>
                    <td>Owner</td>
                    <td>1000</td>
                  </tr>
                </table>
              </div>
            </Col>
            <Col lg={4} xl={3}>
              <div className="py-4">
                <button className="btn btn-custom-primary w-100">
                  save changes
                </button>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    );
  }
}
export default ManageUsers;
