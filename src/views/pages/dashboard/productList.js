/** @format */

import React, { useState, useEffect } from "react";
import { Row, Col, Form, FormGroup, Label, Input, Button } from "reactstrap";
import CardMain from "./cardBlock";

const ProductList = () => {
  const [user, setuser] = useState([]);
  // const [content, setcontent] = useState([]);
  const [title, settitle] = useState("");
  const [body, setbody] = useState("");

  useEffect(() => {
    getuser();
    // return () => {
    //   cleanup
    // }
  }, []);

  const getuser = async () => {
    const data = await fetch("https://jsonplaceholder.typicode.com/posts");
    const resultdata = await data.json();
    // console.log(resultdata);
    const limitdata = resultdata.filter((item, id) => id < 6);
    setuser(limitdata);
    // console.log(user);
  };

  const removeCard = (id) => {
    console.log(user[id]);
    setuser(user.filter((item) => item.id !== id));
    console.log(user);
  };

  const addcard = (e) => {
    e.preventDefault();
    console.log(title, body);
    const newdata = {
      title,
      body,
      id: Math.random() * 2,
    };
    console.log(newdata);
    setuser((user) => [...user, newdata]);
    console.log(user);
    settitle("");
    setbody("");
  };

  const updatedata = (newdata) => {
    setuser((user) =>
      user.map((item) => {
        if (item.id === newdata.id) {
          return newdata;
        } else {
          return item;
        }
      })
    );
  };
  // console.log(user);
  return (
    <div className="">
      <Row className="">
        <Col>
          <h1 className="f-700">Products </h1>
        </Col>
      </Row>
      <Row>
        {user.map((item) => (
          <Col md="4" key={item.id}>
            <CardMain
              updatedata={updatedata}
              userdata={item}
              removeCard={removeCard}
            />
          </Col>
        ))}
      </Row>
      <Row className="my-5">
        <h1>Add Card</h1>
      </Row>
      <Row className="my-5 justify-content-center">
        <Col md="4">
          <div className="add-form">
            <Form onSubmit={(e) => addcard(e)}>
              <FormGroup className="mb-3">
                <Label for="title">Title</Label>
                <Input
                  type="text"
                  name="title"
                  value={title}
                  onChange={(e) => settitle(e.target.value)}
                  id="title"
                  placeholder="title"
                />
              </FormGroup>
              <FormGroup className="mb-3">
                <Label for="examplePassword">Content</Label>
                <Input
                  type="textarea"
                  value={body}
                  onChange={(e) => setbody(e.target.value)}
                  name="bbody"
                  id="bbody"
                  placeholder="bbody"
                />
              </FormGroup>
              <Button
                // onClick={(e) => this.addcard(e)}
                type="submit"
                className="my-2 btn-regular"
              >
                Add Card
              </Button>
            </Form>
          </div>
        </Col>
      </Row>
    </div>
  );
};

export default ProductList;
