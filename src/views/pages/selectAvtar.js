import React, { Component } from 'react'
import { Row, Col, Form, FormGroup, Input } from 'reactstrap'

import Dropzone from 'react-dropzone'

import Select from 'react-select'
import PlacesAutocomplete from 'react-places-autocomplete'
import { connect } from 'react-redux'
import { getInds } from '../../redux/actions/api/apiAction'

class SelectAvtar extends Component {
  state = {
    selectedOption: null,
    address: '',
    ind: [],
    imgData: '',
    imgSrc: ''
  }

  setData = (name, val) => {
    this.setState({ [name]: val })
  }
  componentDidMount = async () => {
    await this.props.getInds()
    let newa = []
    if (this.props.ids.length) {
      for (let index = 0; index < this.props.ids.length; index++) {
        newa.push({
          value: this.props.ids[index].id,
          label: this.props.ids[index].name
        })
      }
    }
    await this.setState({ ind: newa })
  }
  render () {
    return (
      <div>
        <div className='comon-full-layout bring-main-wrapper'>
          <Row className='no-gutters h-100'>
            <Col
              sm={{ size: '12', order: '2' }}
              xl={{ size: '8', order: '1' }}
              lg={{ size: '7', order: '1' }}
              md={{ size: '12', order: '2' }}
              xs={{ size: '12', order: '2' }}
            >
              <div className='subscription-wrapper slice-wrapper'>
                <div className>
                  <h1 className='f-bold'>Welcome! Let’s create your profile</h1>
                  <h5 className='des-tx-fourth'>
                    let others get to know you beter! You can do these later
                  </h5>
                </div>
                <div className='edit-image-portion'>
                  <div className='block-1'>
                    <h3 className='f-bold'>Add an avatar</h3>
                    <div className='drop-input-block'>
                      <Dropzone
                        onDrop={acceptedFiles => console.log(acceptedFiles)}
                      >
                        {({ getRootProps, getInputProps }) => (
                          <section>
                            <div {...getRootProps()}>
                              <input {...getInputProps()} />
                              <p>
                                Drag 'n' drop some files here, or click to
                                select files
                              </p>
                            </div>
                          </section>
                        )}
                      </Dropzone>
                    </div>
                  </div>
                  <div className='or-extra-block'>
                    <span className='or'>
                      <h3 className='f-bold des-tx-black'>OR</h3>
                    </span>
                  </div>
                  <div className='block-2'>
                    <h3 className='f-bold'>Choose Image</h3>
                    <p className='des-tx-primary'>
                      or choose one of the defaults
                    </p>
                    <ul className='list-default-images'>
                      <li className='element-default'>
                        <img
                          src={require('../../assets/images/input-1.svg')}
                          alt='input-1'
                          className='img-fluid'
                        />
                      </li>
                      <li className='element-default'>
                        <img
                          src={require('../../assets/images/input-2.svg')}
                          alt='input-1'
                          className='img-fluid'
                        />
                      </li>
                      <li className='element-default'>
                        <img
                          src={require('../../assets/images/input-3.svg')}
                          alt='input-1'
                          className='img-fluid'
                        />
                      </li>
                      <li className='element-default'>
                        <img
                          src={require('../../assets/images/input-4.svg')}
                          alt='input-1'
                          className='img-fluid'
                        />
                      </li>
                      <li className='element-default'>
                        <img
                          src={require('../../assets/images/input-5.svg')}
                          alt='input-1'
                          className='img-fluid'
                        />
                      </li>
                      <li className='element-default'>
                        <img
                          src={require('../../assets/images/input-6.svg')}
                          alt='input-1'
                          className='img-fluid'
                        />
                      </li>
                    </ul>
                  </div>
                </div>
                <div className='py-3'>
                  <Form>
                    <Row>
                      <Col md={7}>
                        <h3 className='f-bold'>Add your location </h3>
                        <FormGroup className='position-relative'>
                          <PlacesAutocomplete
                            value={this.state.address}
                            onChange={e => this.setData('address', e)}
                            onSelect={this.handleSelect}
                          >
                            {({
                              getInputProps,
                              suggestions,
                              getSuggestionItemProps,
                              loading
                            }) => (
                              <div>
                                <Input
                                  {...getInputProps({
                                    placeholder: 'Search Places ...',
                                    className: 'location-search-input'
                                  })}
                                />
                                <div className='autocomplete-dropdown-container'>
                                  {loading && <div>Loading...</div>}
                                  {suggestions.map(suggestion => {
                                    const className = suggestion.active
                                      ? 'suggestion-item--active'
                                      : 'suggestion-item'
                                    // inline style for demonstration purpose
                                    const style = suggestion.active
                                      ? {
                                          backgroundColor: '#fafafa',
                                          cursor: 'pointer'
                                        }
                                      : {
                                          backgroundColor: '#ffffff',
                                          cursor: 'pointer'
                                        }
                                    return (
                                      <div
                                        {...getSuggestionItemProps(suggestion, {
                                          className,
                                          style
                                        })}
                                      >
                                        <span>{suggestion.description}</span>
                                      </div>
                                    )
                                  })}
                                </div>
                              </div>
                            )}
                          </PlacesAutocomplete>
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={7}>
                        <h3 className='f-bold pt-4'>Add your Industry </h3>
                        <FormGroup className='position-relative custom-select-box'>
                          <Select
                            defaultInputValue={
                              this.state.ind.length ? this.state.ind[0] : ''
                            }
                            value={this.state.selectedOption}
                            onChange={e => this.setData('selectedOption', e)}
                            options={this.state.ind}
                          />
                        </FormGroup>
                      </Col>
                    </Row>
                    <Row>
                      <Col md={4}>
                        <div className='pt-3 pt-md-5'>
                          <button className='btn btn-custom-primary w-100'>
                            Continue
                          </button>
                        </div>
                      </Col>
                    </Row>
                  </Form>
                </div>
              </div>
            </Col>
            <Col
              xl={{ size: '4', order: '2' }}
              lg={{ size: '5', order: '2' }}
              md={{ size: '12', order: '1' }}
              sm={{ size: '12', order: '1' }}
              xs={{ size: '12', order: '1' }}
            >
              <div className='comon-block-1 d-flex justify-space-between'>
                <div className='main-logo'>
                  <img
                    src={require('../../assets/images/logo.svg')}
                    className='img-fluid'
                    alt='logo'
                  />
                </div>
                <div className='image-block pb-0'>
                  <div className>
                    <img
                      src={require('../../assets/images/whatsBring-1.svg')}
                      className='img-fluid'
                      alt='logo-image'
                    />
                  </div>
                </div>
                <div className>
                  <h3 className=' des-tx-white  py-3 py-md-0 text-center'>
                    Lorem Ipsum is simply dummy text of the printing and
                    typesetting industry.
                  </h3>
                </div>
              </div>
            </Col>
          </Row>
        </div>
      </div>
    )
  }
}
const mapStateToProps = state => {
  return {
    ids: state.login.inds
  }
}

export default connect(mapStateToProps, { getInds })(SelectAvtar)
